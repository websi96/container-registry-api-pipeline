#!/bin/bash

# use correct scope here
SCOPE="repository:${FQ_IMAGE_NAME}:delete"

echo $SCOPE

dnf install jq -y

TOKEN=`curl --request GET --user "${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD}" "${CI_SERVER_URL}/jwt/auth?service=container_registry&scope=${SCOPE}" | jq -r .token`

echo $TOKEN

curl --request DELETE \
    --header "Authorization: Bearer ${TOKEN}" \
    --header "Accept: application/vnd.docker.distribution.manifest.v2+json"  \
    "https://${CI_REGISTRY}/v2/${FQ_IMAGE_NAME}/tags/reference/to-be-deleted"
